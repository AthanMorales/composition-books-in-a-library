/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package library;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Mauricio
 */
public class GFC {

    public static void main(String[] args) {
        Book b1 = new Book("Java 1", "Athan");
        List<Book> books = new ArrayList<Book>();
        books.add(b1);

        Library library = new Library(books);
        List<Book> bks = library.getTotalBooksInLibrary();
        for (Book bk : bks) {
            System.out.println("Title: " + bk.getTitle() + "and Author: " + bk.getAuthor());
        }
    }
}
