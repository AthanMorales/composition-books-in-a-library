/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package library;

import java.util.List;

/**
 *
 * @author Mauricio
 */
public class Library {
    private final List<Book> books;
    
    Library (List<Book> books){
        this.books =  books;
    }
    
    public List<Book> getTotalBooksInLibrary(){
        return books;
    }
}
